package com.example

object Main {

  def main(args: Array[String]): Unit = {
    println("\n\n\n/* ********************* HazelcastConnector Start *********************************************** */")
    HazelcastConnectorTest.runCode()
  }
}
