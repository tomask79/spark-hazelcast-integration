name := """apache-spark-2-scala-starter-template"""

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  // HAZELCAST connector dependency
  "com.hazelcast" % "hazelcast-spark" % "0.2",
  //SPARK
  "org.apache.spark" %% "spark-core" % "2.3.0" % Provided,
  "org.apache.spark" %% "spark-sql" % "2.3.0" % Provided,
  "org.apache.spark" %% "spark-hive" % "2.3.0" % Provided,
  "org.apache.spark" %% "spark-streaming" % "2.3.0" % Provided,
  "log4j" % "log4j" % "1.2.17")

assemblyMergeStrategy in assembly := {
 case PathList("META-INF", xs @ _*) => MergeStrategy.discard
 case x => MergeStrategy.first
}

javaOptions in (Test, run) ++= Seq("-Dspark.master=local",
  "-Dlog4j.debug=true",
  "-Dlog4j.configuration=log4j.properties")

outputStrategy := Some(StdoutOutput)

fork := true

coverageEnabled in Test:= true


