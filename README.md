## Howto join Hazelcast IMDG and Spark 2 together ##

At the big data project in which I'm having an opportunity to participate we're holding let's say metadata in the [HBase](https://hbase.apache.org/). 
Which is a very wide used solution especially because of realtime read access. Anyway it's perfectly normal and natural
to have these data contained in the [Hazelcast IMDG](https://hazelcast.org/) for example and access them from [Spark](https://spark.apache.org/) in the RDD way. 
In this repo I'm going to show you howto do so.

### Prerequisities ###

- Spring Boot demo application (starter version 1.5.9) with data stored in the Hazelcast IMap (**hazelcast-app folder**)
- Good old [Hortonworks Sandbox](https://www.cloudera.com/downloads/hortonworks-sandbox.html) 2.6.5 (Because you install it and it works)
- Spark 2 task as fat jar accessing Hazelcast IMap in the RDD way (sbt project in the **spark-hazelcast folder**)
- As connector from Spark 2 to Hazelcast I'll use Greg Luck's [Spark connector for Hazelcast](https://github.com/hazelcast/hazelcast-spark)

#### Spring Boot demo application ####

Core of the hazelcast-app is the [LifecycleListener](https://docs.hazelcast.org/docs/3.8/javadoc/com/hazelcast/core/LifecycleListener.html) which will initialize IMap with the data as soon as the Hazelcast cluster will start.


```java
package com.example;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.LifecycleEvent;
import com.hazelcast.core.LifecycleListener;

/**
 * @author tomask79
 */
public class NodeLifecycleListener implements LifecycleListener {

    private String hazelcastInstanceName;

    /**
     * @param instanceName
     */
    public NodeLifecycleListener(final String instanceName) {
        this.hazelcastInstanceName = instanceName;
    }

    @Override
    public void stateChanged(LifecycleEvent event) {

        switch(event.getState()) {
            case STARTED: {
                System.out.println("Cluster is started, putting test data into distributed map!");
                preloadHZMapOnClusterStart();
                break;
            }
            default: {
                System.out.println(event.toString());
            }
        }
    }

    private void preloadHZMapOnClusterStart() {
        getHazelcastInstance(hazelcastInstanceName).getMap(HZArtifactAPI.PAYMENTS_MAP).
                put("1234HZC", 100.0);
        getHazelcastInstance(hazelcastInstanceName).getMap(HZArtifactAPI.PAYMENTS_MAP).
                put("5344HZC", 1500.0);
        getHazelcastInstance(hazelcastInstanceName).getMap(HZArtifactAPI.PAYMENTS_MAP).
                put("7662HZC", 1300.0);
        getHazelcastInstance(hazelcastInstanceName).getMap(HZArtifactAPI.PAYMENTS_MAP).
                put("8626HZC", 1400.0);
        getHazelcastInstance(hazelcastInstanceName).getMap(HZArtifactAPI.PAYMENTS_MAP).
                put("7277HZC", 1500.0);
        getHazelcastInstance(hazelcastInstanceName).getMap(HZArtifactAPI.PAYMENTS_MAP).
                put("6636HZC", 1500.0);
    }

    private HazelcastInstance getHazelcastInstance(final String name) {
        return Hazelcast.getHazelcastInstanceByName(name);
    }
}
```
to not miss the Hazelcast's [STARTED](https://docs.hazelcast.org/docs/3.8/javadoc/com/hazelcast/core/LifecycleEvent.LifecycleState.html#STARTED) event register the listener directly in the [Config](https://docs.hazelcast.org/docs/latest/javadoc/com/hazelcast/config/Config.html):

```java
    @Bean
    public Config config() {
        Config config = new Config();

        config.setInstanceName(HZArtifactAPI.HAZELCAST_INSTANCE);
        config.setProperty("hazelcast.wait.seconds.before.join","10");

        config.getGroupConfig().setName("mygroup");
        config.getGroupConfig().setPassword("mypassword");

        config.getNetworkConfig().setPortAutoIncrement(true);
        config.getNetworkConfig().setPort(10555);
        config.getNetworkConfig().getJoin().getMulticastConfig().setEnabled(true);

        config.addListenerConfig(
                new ListenerConfig( new NodeLifecycleListener(HZArtifactAPI.HAZELCAST_INSTANCE) ));

        SSLConfig sslConfig = new SSLConfig();
        sslConfig.setEnabled(false);
        config.getNetworkConfig().setSSLConfig(sslConfig);

        return config;
    }

```
Okay now let's compile and run the Spring boot app and test the Hazelcast IMap:

```sh
tomask79:hazelcast-app tomask79$ pwd
/Users/tomask79/workspace/spark-hazelcast-integration/hazelcast-app
tomask79:hazelcast-app tomask79$ mvn clean install
tomask79:hazelcast-app tomask79$ java -jar spring-microservice-service1/target/service1-0.0.1-SNAPSHOT.war
```

to test whether IMap is functional and data are exposed I added simple REST controller so in another terminal run:

```sh
tomask79:hazelcast-app tomask79$ curl http://localhost:8082/payments/7662HZC
1300.0
tomask79:hazelcast-app tomask79$ curl http://localhost:8082/payments/1234HZC
100.0
```

Spring boot demo application ready!

#### Spark 2 task accessing the Hazelcast IMap as RDD ####

When using the Spark Connector for Hazelcast **be sure that you use Hazelcast 3.7.x or higher** as stated in the 
prerequisities. I couldn't connect my Spark 2 task with Hazelcast for some time because I was using 3.6.4.
As a demo I will just get the [RDD](https://spark.apache.org/docs/latest/api/java/org/apache/spark/rdd/RDD.html) on the Hazelcast IMap and run [RDD collect](https://spark.apache.org/docs/latest/api/java/org/apache/spark/rdd/RDD.html#collect--) on it 

```scala
package com.example

import org.apache.spark.{SparkContext, SparkConf}

import org.apache.hadoop.conf.Configuration
import org.apache.spark.sql.{Row, SparkSession}

import com.hazelcast.spark.connector.{toSparkContextFunctions}

object HazelcastConnectorTest {

  def runCode() = {
      val conf = new SparkConf()
          .set("hazelcast.server.addresses", "127.0.0.1:10555")
          .set("hazelcast.server.groupName", "mygroup")
          .set("hazelcast.server.groupPass", "mypassword")
          .set("hazelcast.spark.valueBatchingEnabled", "true")
          .set("hazelcast.spark.readBatchSize", "5000")
          .set("hazelcast.spark.writeBatchSize", "5000")

      val sc = new SparkContext(conf)

      val rddFromMap = sc.fromHazelcastMap("payments_map")

      rddFromMap.collect().foreach(println)
  }
}
```
where IMap name **"payments_map" equals to HZArtifactAPI.PAYMENTS_MAP constant** in already mentioned NodeLifecycleListener.

#### Testing everything together ####

Repository has two folders one with the hazelcast-app which is a **maven project** then a spark scala **sbt project**:

**Building hazelcast-app:**

```sh
tomask79:hazelcast-app tomask79$ pwd
/Users/tomask79/workspace/spark-hazelcast-integration/hazelcast-app
tomask79:hazelcast-app tomask79$ mvn clean install
```

**Building Spark sbt project:**

```sh
tomask79:spark-hazelcast tomask79$ pwd
/Users/tomask79/workspace/spark-hazelcast-integration/spark-hazelcast
tomask79:spark-hazelcast tomask79$ sbt assembly
```

Now let's start the [Hortonworks Sandbox](https://www.cloudera.com/downloads/hortonworks-sandbox.html) and upload the Spark fat jar into it:

```sh
tomask79:scala-2.11 tomask79$ pwd
/Users/tomask79/workspace/spark-hazelcast-integration/spark-hazelcast/target/scala-2.11
tomask79:scala-2.11 tomask79$ scp -P 2222 apache-spark-2-scala-starter-template-assembly-1.0.jar root@localhost:/root
root@localhost's password: 
apache-spark-2-scala-starter-template-assembly-1.0.jar                                                                                       100%   13MB  42.8MB/s   00:00    
```

Now if you use VirtualBox for Sandbox and you want to access Hazelcast application running outside then you have to set the port
forwarding. I didn't want to mess with it so I uploaded hazelcast-app WAR file into sandbox as well...It's up to you.

**Starting the hazelcast-app:**

```sh
[root@sandbox-hdp ~]# java -jar service1-0.0.1-SNAPSHOT.war 
```
again let's test it in another terminal whether it works:

```sh
[root@sandbox-hdp ~]# curl http://localhost:8082/payments/7662HZC
1300.0 
```
looks good...

**Starting the Spark 2 task accessing the Hazelcast IMDG**

```sh
[root@sandbox-hdp ~]# spark-submit --class com.example.Main --master yarn-client apache-spark-2-scala-starter-template-assembly-1.0.jar 
```

and in the output you should see content of the IMap "payments_map" visible in the Spark 2:

```sh
19/09/01 20:10:16 INFO YarnScheduler: Removed TaskSet 0.0, whose tasks have all completed, from pool
19/09/01 20:10:16 INFO DAGScheduler: ResultStage 0 (collect at HazelcastConnectorTest.scala:25) finished in 12.410 s
19/09/01 20:10:16 INFO DAGScheduler: Job 0 finished: collect at HazelcastConnectorTest.scala:25, took 12.682620 s
(7277HZC,1500.0)
(8626HZC,1400.0)
(5344HZC,1500.0)
(1234HZC,100.0)
(7662HZC,1300.0)
(6636HZC,1500.0)
```

[Spark connector for Hazelcast](https://github.com/hazelcast/hazelcast-spark) seems to be working.
Great, If you've got some data in Hazelcast IMDG and you need to use them in your Big Data Hadoop Warhouse processing 
then this connector is very useful I think.

Best Regards

Tomas