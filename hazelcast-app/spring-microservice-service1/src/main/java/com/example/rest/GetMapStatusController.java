package com.example.rest;

import com.example.HZArtifactAPI;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * @author tomask79
 */
@RestController
public class GetMapStatusController {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @GetMapping("/payments/{transaction}")
    private Double getAmount(@PathVariable("transaction") String transaction) {
        System.out.println("Getting amount for transactions: "+transaction);
        return (Double) hazelcastInstance.getMap(HZArtifactAPI.PAYMENTS_MAP).get(transaction);
    }
}
